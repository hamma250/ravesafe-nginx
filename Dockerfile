FROM nginx:mainline
RUN rm /etc/nginx/nginx.conf /etc/nginx/conf.d/default.conf
COPY nginx.conf /etc/nginx/nginx.conf
COPY php_fastcgi.conf /etc/nginx/conf.d/php_fastcgi.conf
COPY nofile.conf /etc/security/limits.d/nofile.conf
